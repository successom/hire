Mysterious API application.


use rake db:seed after migrations
it creates users and some data in the database

username: 'admin', password: 'admin', role: 'admin'
username: 'user', password: 'user', role: 'user',
username: 'guest', password: 'guest', role: 'guest'



You can show application routes using command:
  rake grape:routes


Examples of implemented REST requests:

auth:

	log in (get token)
		curl -i -X POST -d "username=user&password=user" "http://localhost:3000/api/v1/sessions"

	log out (update auth token)
		curl -i -X DELETE -d "authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d" "http://localhost:3000/api/v1/sessions"

Vacancy model:
	get vacancies
		curl -i -X GET "http://localhost:3000/api/v1/vacancies?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	create vacancy
		curl -i -X POST -d "name=HR Manager&validity_period=12&salary=1000&contact_information=Ukraine, Kharkiv" "http://localhost:3000/api/v1/vacancies?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	get vacancy by id
		curl -i -X GET "http://localhost:3000/api/v1/vacancies/4?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	update vacancy by id
		curl -i -X PUT -d "name=Delivery director" "http://localhost:3000/api/v1/vacancies/4?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	delete vacancy by id
		curl -i -X DELETE "http://localhost:3000/api/v1/vacancies/4?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"


Skill model:
	get skills
		curl -i -X GET "http://localhost:3000/api/v1/vacancies/4/skills?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	create skill
		curl -i -X POST -d "name=automatic testing" "http://localhost:3000/api/v1/vacancies/4/skills?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	get skill by id
		curl -i -X GET "http://localhost:3000/api/v1/vacancies/4/skills/7?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"

	update skill by id
		curl -i -X PUT -d "name=dream" "http://localhost:3000/api/v1/vacancies/4/skills/7?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"


	delete skill by id
		curl -i -X DELETE "http://localhost:3000/api/v1/vacancies/4/skills/7?authentication_token=2b37d67f8f724f4ba1f9c1b673a2049d"
