# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.delete_all
users     = [{username: 'admin', password: 'admin', role: 'admin'},
	         {username: 'user', password: 'user', role: 'user'},
	         {username: 'guest', password: 'guest', role: 'guest'}
            ]

users.each do |user|
  User.create!(user)
end


Vacancy.delete_all
user_id = User.find_by_username('admin').id
vacancies = [{name: 'DB developer', validity_period: 2, user_id: user_id,
              salary: 1500, contact_information: 'DB development company'},
             {name: 'Java developer', validity_period: 3, user_id: user_id,
              salary: 2500, contact_information: 'Banks technology company'},
             {name: 'Ruby developer', validity_period: 5, user_id: user_id,
              salary: 3500, contact_information: 'WEB development company'}
            ]

vacancies.each do |vacancy|
  Vacancy.create!(vacancy)
end

Skill.delete_all
skills = [{name: 'PL/SQL', vacancy_id: Vacancy.find_by_name('DB developer').id},
          {name: 'DDL & DML', vacancy_id: Vacancy.find_by_name('DB developer').id},
          {name: 'OOP programming', vacancy_id: Vacancy.find_by_name('Java developer').id},
          {name: 'Creating unit tests', vacancy_id: Vacancy.find_by_name('Java developer').id},
          {name: 'Functional programming', vacancy_id: Vacancy.find_by_name('Ruby developer').id},
          {name: 'TDD & BDD', vacancy_id: Vacancy.find_by_name('Ruby developer').id}
         ]

skills.each do |skill|
  Skill.create!(skill)
end
