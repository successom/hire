class AddUserToVacancy < ActiveRecord::Migration
  def change
    add_reference :vacancies, :user, index: true
  end
end
