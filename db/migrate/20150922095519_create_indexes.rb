class CreateIndexes < ActiveRecord::Migration
  def change
    add_index(:vacancies, :id)
    add_index(:skills, :id)
    add_index(:skills, :vacancy_id)
  end
end
