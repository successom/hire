# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151006064238) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "skills", force: true do |t|
    t.string  "name"
    t.integer "vacancy_id"
  end

  add_index "skills", ["id"], name: "index_skills_on_id", using: :btree
  add_index "skills", ["vacancy_id"], name: "index_skills_on_vacancy_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "username",             default: "", null: false
    t.string   "encrypted_password",   default: "", null: false
    t.string   "authentication_token"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "role"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["id"], name: "index_users_on_id", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "vacancies", force: true do |t|
    t.string   "name"
    t.integer  "validity_period"
    t.integer  "salary"
    t.text     "contact_information"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "vacancies", ["id"], name: "index_vacancies_on_id", using: :btree
  add_index "vacancies", ["user_id"], name: "index_vacancies_on_user_id", using: :btree

end
