FactoryGirl.define do
  factory :vacancy do
    sequence(:name){ |n| "name_#{n}" }
    validity_period 6
    salary 1000
    sequence(:contact_information){ |n| "address_#{n}" }
    user { User.first }
  end
end
