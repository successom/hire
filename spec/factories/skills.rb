FactoryGirl.define do
  factory :skill do
    sequence(:name){ |n| "skill_##{n}" }
    vacancy { FactoryGirl.create(:vacancy) }
  end
end
