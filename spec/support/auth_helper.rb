module AuthHelper
  def get_credentials(user = nil)
    user ||= create :user
    {authentication_token: user.authentication_token, username: user.username}
  end
end
