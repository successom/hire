require 'spec_helper'

describe V1::Vacancies do
  let(:params) do
    {
      name:                 attributes_for(:vacancy)[:name],
      validity_period:      attributes_for(:vacancy)[:validity_period],
      salary:               attributes_for(:vacancy)[:salary],
      contact_information:  attributes_for(:vacancy)[:contact_information]
    }
  end

  let(:user) { create :user }
  let!(:users_vacancy) { create :vacancy, user: user }
  let(:users_params) { params.merge! @users_credentials }

  let(:admin) { create :user, role: 'admin' }
  let!(:admins_vacancy) { create :vacancy, user: admin }
  let(:admins_params) { params.merge! @admins_credentials }

  let(:guest) { create :user, role: 'guest' }
  let(:another_user) { create :user, role: 'user' }
  let!(:another_users_vacancy) { create :vacancy, user: another_user }
  let(:guests_params) { params.merge! @guests_credentials }

  before do
    @users_credentials          = get_credentials(user)
    @admins_credentials         = get_credentials(admin)
    @guests_credentials         = get_credentials(guest)
    @another_users_credentials  = get_credentials(another_user)
  end

  def response_json
    JSON.parse(response.body)
  end

  context 'admin' do
    describe "can GET" do
      it "all list of vacancies" do
        get "/api/v1/vacancies", @admins_credentials

        expect(response.status).to eql 200
        expect(response_json.count).to eq Vacancy.count
      end

      it "vacancy another users by id" do
        get "/api/v1/vacancies/#{users_vacancy.id}", @admins_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => users_vacancy.id,
          "name" => users_vacancy.name,
          "validity_period" => users_vacancy.validity_period,
          "salary" => users_vacancy.salary,
          "contact_information" => users_vacancy.contact_information,
          "user_id" => users_vacancy.user_id
        })
      end

      it "own vacancy by id" do
        get "/api/v1/vacancies/#{admins_vacancy.id}", @admins_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => admins_vacancy.id,
          "name" => admins_vacancy.name,
          "validity_period" => admins_vacancy.validity_period,
          "salary" => admins_vacancy.salary,
          "contact_information" => admins_vacancy.contact_information,
          "user_id" => admins_vacancy.user_id
        })
      end
    end

    describe "can POST" do
      it "new vacancy" do
        post '/api/v1/vacancies', admins_params
        vacancy = Vacancy.last

        expect(response_json['vacancy']).to eq({
          "id"   => vacancy.id,
          "name" => vacancy.name,
          "validity_period" => vacancy.validity_period,
          "salary" => vacancy.salary,
          "contact_information" => vacancy.contact_information,
          "user_id" => vacancy.user_id
        })
      end
    end

    describe "can UPDATE" do
      it "own vacancy" do
        admins_params['validity_period'] = 12
        admins_params['salary'] = 10
        put "/api/v1/vacancies/#{admins_vacancy.id}", admins_params

        expect(response_json['vacancy']).to include({
          "id"   => admins_vacancy.id,
          "validity_period" => admins_params['validity_period'],
          "salary" => admins_params['salary']
        })
      end

      it "another users vacancy" do
        admins_params['validity_period'] = 12
        admins_params['salary'] = 10
        put "/api/v1/vacancies/#{another_users_vacancy.id}", admins_params

        expect(response_json['vacancy']).to include({
          "id"   => another_users_vacancy.id,
          "validity_period" => admins_params['validity_period'],
          "salary" => admins_params['salary']
        })
      end
    end

    describe "can DELETE" do
      it "own vacancy" do
        delete "/api/v1/vacancies/#{admins_vacancy.id}", @admins_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => admins_vacancy.id,
          "message" => "vacancy ID = #{admins_vacancy.id} has been destroyed"
        })
      end

      it "another users vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}", @admins_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => another_users_vacancy.id,
          "message" => "vacancy ID = #{another_users_vacancy.id} has been destroyed"
        })
      end
    end
  end

  context 'ordinary user' do
    describe "can GET" do
      it "all list of vacancies" do
        get "/api/v1/vacancies", @users_credentials

        expect(response.status).to eql 200
        expect(response_json.count).to eq Vacancy.count
      end

      it "vacancy another users by id" do
        get "/api/v1/vacancies/#{admins_vacancy.id}", @users_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => admins_vacancy.id,
          "name" => admins_vacancy.name,
          "validity_period" => admins_vacancy.validity_period,
          "salary" => admins_vacancy.salary,
          "contact_information" => admins_vacancy.contact_information,
          "user_id" => admins_vacancy.user_id
        })
      end

      it "own vacancy by id" do
        get "/api/v1/vacancies/#{users_vacancy.id}", @users_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => users_vacancy.id,
          "name" => users_vacancy.name,
          "validity_period" => users_vacancy.validity_period,
          "salary" => users_vacancy.salary,
          "contact_information" => users_vacancy.contact_information,
          "user_id" => users_vacancy.user_id
        })
      end
    end

    describe "can POST" do
      it "new vacancy" do
        post '/api/v1/vacancies', users_params
        vacancy = Vacancy.last

        expect(response_json['vacancy']).to eq({
          "id"   => vacancy.id,
          "name" => vacancy.name,
          "validity_period" => vacancy.validity_period,
          "salary" => vacancy.salary,
          "contact_information" => vacancy.contact_information,
          "user_id" => vacancy.user_id
        })
      end
    end

    describe "can UPDATE" do
      it "own vacancy" do
        users_params['validity_period'] = 12
        users_params['salary'] = 10
        put "/api/v1/vacancies/#{users_vacancy.id}", users_params

        expect(response_json['vacancy']).to include({
          "id"   => users_vacancy.id,
          "validity_period" => users_params['validity_period'],
          "salary" => users_params['salary']
        })
      end
    end

    describe "can NOT UPDATE" do
      it "another users vacancy" do
        users_params['validity_period'] = 12
        users_params['salary'] = 10
        put "/api/v1/vacancies/#{another_users_vacancy.id}", users_params

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can DELETE" do
      it "own vacancy" do
        delete "/api/v1/vacancies/#{users_vacancy.id}", @users_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => users_vacancy.id,
          "message" => "vacancy ID = #{users_vacancy.id} has been destroyed"
        })
      end
    end

    describe "can NOT DELETE" do
      it "another users vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}", @users_credentials

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end
  end

  context 'guest' do
    describe "can GET" do
      it "all list of vacancies" do
        get "/api/v1/vacancies", @guests_credentials

        expect(response.status).to eql 200
        expect(response_json.count).to eq Vacancy.count
      end

      it "vacancy another users by id" do
        get "/api/v1/vacancies/#{admins_vacancy.id}", @guests_credentials

        expect(response_json['vacancy']).to eq({
          "id"   => admins_vacancy.id,
          "name" => admins_vacancy.name,
          "validity_period" => admins_vacancy.validity_period,
          "salary" => admins_vacancy.salary,
          "contact_information" => admins_vacancy.contact_information,
          "user_id" => admins_vacancy.user_id
        })
      end
    end

    describe "can NOT POST" do
      it "new vacancy" do
        post '/api/v1/vacancies', guests_params

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can NOT UPDATE" do
      it "another users vacancy" do
        guests_params['validity_period'] = 12
        guests_params['salary'] = 10
        put "/api/v1/vacancies/#{another_users_vacancy.id}", guests_params

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can NOT DELETE" do
      it "another users vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}", @guests_credentials

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end
  end

  context 'exceptions' do
    describe "Unauthorized" do
      it "returns 401 status" do
        get "/api/v1/vacancies"
        expect(response.status).to eql 401
      end
    end

    describe "Validation" do
      it "returns validation message" do
        users_params['validity_period'] = nil
        post "/api/v1/vacancies", users_params
        expect(response_json['errors']).to eql("validity_period is empty")
      end
    end

    describe "ActiveRecord" do
      it "RecordNotFound message" do
        get "/api/v1/vacancies/100", @users_credentials
        expect(response_json['errors']).to eql("Couldn't find Vacancy with 'id'=100")
      end
    end
  end
end
