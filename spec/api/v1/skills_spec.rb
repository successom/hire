require 'spec_helper'

describe V1::Vacancies do
  let(:params) do
    {
      name: attributes_for(:skill)[:name]
    }
  end

  let(:user) { create :user }
  let(:users_vacancy) { create :vacancy, user: user }
  let!(:users_vacancy_skill) { create :skill, vacancy: users_vacancy }
  let(:users_params) { params.merge! @users_credentials }

  let(:admin) { create :user, role: 'admin' }
  let(:admins_vacancy) { create :vacancy, user: admin }
  let!(:admins_vacancy_skill) { create :skill, vacancy: admins_vacancy }
  let(:admins_params) { params.merge! @admins_credentials }

  let(:guest) { create :user, role: 'guest' }
  let(:another_user) { create :user, role: 'user' }
  let(:another_users_vacancy) { create :vacancy, user: another_user }
  let!(:another_users_vacancy_skill) { create :skill, vacancy: another_users_vacancy }
  let(:guests_params) { params.merge! @guests_credentials }

  before do
    @users_credentials          = get_credentials(user)
    @admins_credentials         = get_credentials(admin)
    @guests_credentials         = get_credentials(guest)
    @another_users_credentials  = get_credentials(another_user)
  end

  def response_json
    JSON.parse(response.body)
  end

  context 'admin' do
    describe "can GET" do
      it "list of skills for own vacancy" do
        get "/api/v1/vacancies/#{admins_vacancy.id}/skills", @admins_credentials

        expect(response.status).to eql 200
        expect(response_json).to eq ({"id" => admins_vacancy_skill.id, "name" => admins_vacancy_skill.name, "vacancy_id" => admins_vacancy.id})
      end

      it "list of skills for anoter users vacancy" do
        get "/api/v1/vacancies/#{another_users_vacancy.id}/skills", @admins_credentials

        expect(response.status).to eql 200
        expect(response_json).to eq ({"id" => another_users_vacancy_skill.id, "name" => another_users_vacancy_skill.name, "vacancy_id" => another_users_vacancy.id})
      end

      it "skill for anoter users vacancy by id" do
        get "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", @admins_credentials

        expect(response_json['skill']).to eq({
          "id"   => another_users_vacancy_skill.id,
          "name" => another_users_vacancy_skill.name,
          "vacancy_id" => another_users_vacancy.id
        })
      end

      it "skill for own vacancy by id" do
        get "/api/v1/vacancies/#{admins_vacancy.id}/skills/#{admins_vacancy_skill.id}", @admins_credentials

        expect(response_json['skill']).to eq({
          "id"   => admins_vacancy_skill.id,
          "name" => admins_vacancy_skill.name,
          "vacancy_id" => admins_vacancy.id
        })
      end
    end

    describe "can POST" do
      it "new skill for own vacancy" do
        post "/api/v1/vacancies/#{admins_vacancy.id}/skills", admins_params
        skill = Skill.last

        expect(response_json['skill']).to eq({
          "id"         => skill.id,
          "name"       => skill.name,
          "vacancy_id" => admins_vacancy.id
        })
      end

      it "new skill for another users vacancy" do
        post "/api/v1/vacancies/#{another_users_vacancy.id}/skills", admins_params
        skill = Skill.last

        expect(response_json['skill']).to eq({
          "id"         => skill.id,
          "name"       => skill.name,
          "vacancy_id" => another_users_vacancy.id
        })
      end
    end

    describe "can UPDATE" do
      it "skill for own vacancy" do
        admins_params['name'] = 'Changed skill'
        put "/api/v1/vacancies/#{admins_vacancy.id}/skills/#{admins_vacancy_skill.id}", admins_params

        expect(response_json['skill']).to eq({
          "id"   => admins_vacancy_skill.id,
          "name" => admins_params['name'],
          "vacancy_id" => admins_vacancy.id
        })
      end
    end

    describe "can UPDATE" do
      it "skill for another users vacancy" do
        admins_params['name'] = 'Changed skill'
        put "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", admins_params

        expect(response_json['skill']).to eq({
          "id"   => another_users_vacancy_skill.id,
          "name" => admins_params['name'],
          "vacancy_id" => another_users_vacancy.id
        })
      end
    end

    describe "can DELETE" do
      it "skill for own vacancy" do
        delete "/api/v1/vacancies/#{admins_vacancy.id}/skills/#{admins_vacancy_skill.id}", @admins_credentials

        expect(response_json['skill']).to eq({
          "id"   => admins_vacancy_skill.id,
          "message" => "skill ID = #{admins_vacancy_skill.id} has been destroyed"
        })
      end
    end

    describe "can DELETE" do
      it "skill for another users vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", @admins_credentials

        expect(response_json['skill']).to eq({
          "id"   => another_users_vacancy_skill.id,
          "message" => "skill ID = #{another_users_vacancy_skill.id} has been destroyed"
        })
      end
    end
  end

  context 'ordinary user' do
    describe "can GET" do
      it "list of skills for own vacancy" do
        get "/api/v1/vacancies/#{users_vacancy.id}/skills", @users_credentials

        expect(response.status).to eql 200
        expect(response_json).to eq ({"id" => users_vacancy_skill.id, "name" => users_vacancy_skill.name, "vacancy_id" => users_vacancy.id})
      end

      it "list of skills for anoter users vacancy" do
        get "/api/v1/vacancies/#{another_users_vacancy.id}/skills", @users_credentials

        expect(response.status).to eql 200
        expect(response_json).to eq ({"id" => another_users_vacancy_skill.id, "name" => another_users_vacancy_skill.name, "vacancy_id" => another_users_vacancy.id})
      end

      it "skill for anoter users vacancy by id" do
        get "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", @users_credentials

        expect(response_json['skill']).to eq({
          "id"   => another_users_vacancy_skill.id,
          "name" => another_users_vacancy_skill.name,
          "vacancy_id" => another_users_vacancy.id
        })
      end

      it "skill for own vacancy by id" do
        get "/api/v1/vacancies/#{users_vacancy.id}/skills/#{users_vacancy_skill.id}", @users_credentials

        expect(response_json['skill']).to eq({
          "id"   => users_vacancy_skill.id,
          "name" => users_vacancy_skill.name,
          "vacancy_id" => users_vacancy.id
        })
      end
    end

    describe "can POST" do
      it "new skill for own vacancy" do
        post "/api/v1/vacancies/#{users_vacancy.id}/skills", users_params
        skill = Skill.last

        expect(response_json['skill']).to eq({
          "id"         => skill.id,
          "name"       => skill.name,
          "vacancy_id" => users_vacancy.id
        })
      end
    end

    describe "can NOT POST" do
      it "new skill for another users vacancy" do
        post "/api/v1/vacancies/#{another_users_vacancy.id}/skills", users_params
        skill = Skill.last

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can UPDATE" do
      it "skill for own vacancy" do
        users_params['name'] = 'Changed skill'
        put "/api/v1/vacancies/#{users_vacancy.id}/skills/#{users_vacancy_skill.id}", users_params

        expect(response_json['skill']).to eq({
          "id"   => users_vacancy_skill.id,
          "name" => users_params['name'],
          "vacancy_id" => users_vacancy.id
        })
      end
    end

    describe "can NOT UPDATE" do
      it "skill for another users vacancy" do
        users_params['name'] = 'Changed skill'
        put "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", users_params

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can DELETE" do
      it "skill for own vacancy" do
        delete "/api/v1/vacancies/#{users_vacancy.id}/skills/#{users_vacancy_skill.id}", @users_credentials

        expect(response_json['skill']).to eq({
          "id"   => users_vacancy_skill.id,
          "message" => "skill ID = #{users_vacancy_skill.id} has been destroyed"
        })
      end
    end

    describe "can NOT DELETE" do
      it "skill for another users vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", @users_credentials

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end
  end

  context 'guest' do
    describe "can GET" do
      it "list of skills for another users vacancy" do
        get "/api/v1/vacancies/#{users_vacancy.id}/skills", @guests_credentials

        expect(response.status).to eql 200
        expect(response_json).to eq ({"id" => users_vacancy_skill.id, "name" => users_vacancy_skill.name, "vacancy_id" => users_vacancy.id})
      end

      it "skill for anoter users vacancy by id" do
        get "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", @guests_credentials

        expect(response_json['skill']).to eq({
          "id"   => another_users_vacancy_skill.id,
          "name" => another_users_vacancy_skill.name,
          "vacancy_id" => another_users_vacancy.id
        })
      end
    end

    describe "can NOT POST" do
      it "new skill for another users vacancy" do
        post "/api/v1/vacancies/#{another_users_vacancy.id}/skills", guests_params

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can NOT UPDATE" do
      it "skill for another users vacancy" do
        guests_params['name'] = 'Changed skill'
        put "/api/v1/vacancies/#{users_vacancy.id}/skills/#{users_vacancy_skill.id}", guests_params

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end

    describe "can NOT DELETE" do
      it "skill for another users vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{another_users_vacancy_skill.id}", @guests_credentials

        expect(response.status).to eql 403
        expect(response_json['error']).to eql('403 Access denied')
      end
    end
  end

  context 'exceptions' do
    describe "Unauthorized" do
      it "returns 401 status" do
        get "/api/v1/vacancies/#{users_vacancy.id}/skills/users_vacancy_skill"
        expect(response.status).to eql 401
      end
    end

    describe "Validation" do
      it "returns validation message" do
        users_params['name'] = nil
        post "/api/v1/vacancies/#{users_vacancy.id}/skills", users_params
        expect(response_json['errors']).to eql("name is empty")
      end
    end

    describe "ActiveRecord" do
      it "not found message" do
        get "/api/v1/vacancies/#{users_vacancy.id}/skills/1000", @users_credentials
        expect(response_json['errors']).to eql("ActiveRecord::RecordNotFound")
      end

      it "user can't update skill that not belongs to vacancy" do
        users_params['name'] = 'Changed skill'
        put "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{users_vacancy_skill.id}", users_params
        expect(response_json['errors']).to eql("ActiveRecord::RecordNotFound")
      end

      it "user can't delete skill that not belongs to vacancy" do
        delete "/api/v1/vacancies/#{another_users_vacancy.id}/skills/#{users_vacancy_skill.id}", users_params
        expect(response_json['errors']).to eql("ActiveRecord::RecordNotFound")
      end
    end
  end
end
