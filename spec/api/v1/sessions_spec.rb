require 'spec_helper'

describe V1::Skills do
  let(:params) do
    {
      username: 'user',
      password: 'user'
    }
  end

  before(:all) do
    @user = create :user, username: 'user', password: 'user'
    @credentials = get_credentials(@user)
  end

  def response_json
    JSON.parse(response.body)
  end

  describe "POST /api/v1/sessions" do
    it "returns users token" do
      post '/api/v1/sessions', params

      expect(response_json['token']).to eq(@user[:authentication_token])
    end
  end

  describe "DELETE /api/v1/sessions/" do
    it "updates users token" do
      authentication_token = @user.authentication_token
      delete "/api/v1/sessions", @credentials

      expect(@user.reload.authentication_token).to_not eq(authentication_token)
      expect(response_json['notice']).to eq('You need to sign in before continuing.')
    end
  end
end
