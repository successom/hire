class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.role? :admin
      can :manage, :all
    elsif user.role? :user
      can :manage, Vacancy, user_id: user.id
      can :manage, Skill, vacancy: { user_id: user.id }
    elsif  user.role? :guest
      can :read, :all
    end
  end
end
