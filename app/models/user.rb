class User < ActiveRecord::Base

  attr_accessor :password

  validates :username, presence: true, uniqueness: true
  validates_presence_of :password, :on => :create

  before_save :ensure_authentication_token, :encrypted_password

  has_many :vacancies

  def email_required?
   false
  end

  def email_changed?
    false
  end

  def role?(user_role)
    role == user_role.to_s
  end

  def encrypted_password
    if password.present?
      self.encrypted_password = Digest::SHA1.hexdigest(password)
    end
  end

  private

  def ensure_authentication_token
    self.authentication_token = generate_authentication_token
  end

  def generate_authentication_token
    loop do
      token = SecureRandom.uuid.gsub(/\-/,'')
      break token unless User.where(authentication_token: token).first
    end
  end

  def self.authenticated(username, password)
    user = find_by_username(username)
    if user && user[:encrypted_password] == Digest::SHA1.hexdigest(password)
      user
    else
      nil
    end
  end
end
