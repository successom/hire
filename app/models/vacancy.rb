class Vacancy < ActiveRecord::Base
  has_many :skills, dependent: :destroy
  belongs_to :user
  validates_presence_of :name, :validity_period, :salary, :contact_information, on: :create
end
