class Skill < ActiveRecord::Base
  belongs_to :vacancy
  validates_presence_of :name
end
