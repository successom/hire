module V1
  class Skills < Grape::API
    namespace 'vacancies/:vacancy_id' do
      namespace :skills do
        get do
          if requested_user.can? :read, Skill
            @skill = Skill.find_by_vacancy_id(params[:vacancy_id])
          else
            error!("403 Access denied", 403)
          end
        end

        params do
          requires :name, type: String, allow_blank: false
        end

        post do
          if (requested_user.can? :create, Skill) && (requested_user.can? :update, Vacancy.find(params[:vacancy_id]))
            @skill = SkillCreator.new(params).execute
          else
            error!("403 Access denied", 403)
          end
        end

        params do
          requires :name, type: String, allow_blank: false
        end

        put ":id" do
          if requested_user.can? :update, Skill.find(params[:id])
            @skill = SkillEditor.new(params).execute
          else
            error!("403 Access denied", 403)
          end
        end

        get ":id" do
          if requested_user.can? :read, Skill
            @skill = SkillSearcher.new(params).execute
          else
            error!("403 Access denied", 403)
          end
        end

        delete ":id" do
          if requested_user.can? :destroy, Skill.find(params[:id])
            @skill = SkillDestroyer.new(params).execute
          else
            error!("403 Access denied", 403)
          end
        end
      end
    end
  end
end
