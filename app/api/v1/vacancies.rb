module V1
  class Vacancies < Grape::API
    namespace :vacancies do
      get do
        if requested_user.can? :read, Vacancy
          @vacancy = Vacancy.all
        else
          error!("403 Access denied", 403)
        end
      end

      params do
        requires :name, type: String, allow_blank: false
        requires :validity_period, type: Integer, allow_blank: false
        requires :salary, type: Integer, allow_blank: false
        requires :contact_information, type: String, allow_blank: false
      end

      post do
        if requested_user.can? :create, Vacancy
          @vacancy = VacancyCreator.new(params, current_user).execute
        else
          error!("403 Access denied", 403)
        end
      end

      put ":id" do
        if requested_user.can? :update, Vacancy.find(params[:id])
          @vacancy = VacancyEditor.new(params).execute
        else
          error!("403 Access denied", 403)
        end
      end

      get ":id" do
        if requested_user.can? :read, Vacancy
          @vacancy = VacancySearcher.new(params).execute
        else
          error!("403 Access denied", 403)
        end
      end

      delete ":id" do
        if requested_user.can? :destroy, Vacancy.find(params[:id])
          @vacancy = VacancyDestroyer.new(params).execute
        else
           error!("403 Access denied", 403)
        end
      end
    end
  end
end
