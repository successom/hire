module V1
  class Api < Grape::API

    version 'v1', using: :path
    format :json
    default_format :json

    rescue_from ActiveRecord::RecordNotFound do |e|
      message = e.message.gsub(/\s*\[.*\Z/, '')
      Rack::Response.new(
          { success: false,
             status: 404,
             message: "not_found",
             errors: message }.to_json, 404,
          { 'Content-Type' => 'application/json' })
      end

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      Rack::Response.new(
          { success: false,
             status: 422,
             message: 'validation error',
             errors: e.message }.to_json, 422,
          { 'Content-Type' => 'application/json' })
    end

    before do
      unless authenticated || getting_token
        error!("401 Unauthorized", 401)
      end
    end

    helpers do
      def authenticated
        params[:authentication_token] && @user = User.find_by_authentication_token(params[:authentication_token])
      end

      def getting_token
        params[:username].present? && params[:password].present?
      end

      def current_user
        User.find_by_authentication_token(params[:authentication_token]) || @user
      end

      def requested_user
        Ability.new(current_user)
      end
    end

    # resources:
    mount V1::Sessions
    mount V1::Vacancies
    mount V1::Skills
  end
end
