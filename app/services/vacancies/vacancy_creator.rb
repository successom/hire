class VacancyCreator
  def initialize(params, current_user)
    @params = {
      name:                params[:name],
      validity_period:     params[:validity_period],
      salary:              params[:salary],
      contact_information: params[:contact_information],
      user_id:             current_user.id
    }
  end

  def execute
    if vacancy.save
      {
        vacancy: {
          id:                  vacancy.id,
          name:                vacancy.name,
          validity_period:     vacancy.validity_period,
          salary:              vacancy.salary,
          contact_information: vacancy.contact_information,
          user_id:             vacancy.user_id
        }
      }
    else
      { errors: vacancy.errors.messages }
    end
  end

  private

  def vacancy
    @vacancy ||= Vacancy.new(@params)
  end
end
