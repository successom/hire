class VacancyDestroyer
  def initialize(params)
    @params = { id: params[:id] }
  end

  def execute
    if vacancy.destroy
      {
        vacancy: {
          id: vacancy.id,
          message: "vacancy ID = #{vacancy.id} has been destroyed"
        }
      }
    else
      { errors: vacancy.errors.messages }
    end
  end

  private

  def vacancy
    @vacancy ||= Vacancy.find(@params[:id])
  end
end
