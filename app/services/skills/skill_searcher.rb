class SkillSearcher
  def initialize(params)
    @params = { id:         params[:id],
                vacancy_id: params[:vacancy_id]
    }
  end

  def execute
    if skill.present?
      {
        skill: {
          id:         skill.id,
          name:       skill.name,
          vacancy_id: skill.vacancy_id
        }
      }
    else
      { errors: skill.errors.messages }
    end
  end

  private

  def skill
    @skill ||= Skill.find_by!(id: @params[:id], vacancy_id: @params[:vacancy_id])
  end
end
