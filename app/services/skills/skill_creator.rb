class SkillCreator
  def initialize(params)
    @params = {
      name:       params[:name],
      vacancy_id: params[:vacancy_id]
    }
  end

  def execute
    if skill.save
      {
        skill: {
          id:         skill.id,
          name:       skill.name,
          vacancy_id: skill.vacancy_id
        }
      }
    else
      { errors: skill.errors.messages }
    end
  end

  private

  def skill
    @skill ||= Skill.new(name: @params[:name], vacancy_id: vacancy_id)
  end

  def vacancy_id
    @vacancy_id ||= Vacancy.find(@params[:vacancy_id]).id
  end
end
