class SkillDestroyer
  def initialize(params)
    @params = { id:         params[:id],
                vacancy_id: params[:vacancy_id]
    }
  end

  def execute
    if skill.destroy
      {
        skill: {
          id: skill.id,
          message: "skill ID = #{skill.id} has been destroyed"
        }
      }
    else
      { errors: skill.errors.messages }
    end
  end

  private

  def skill
    @skill ||= Skill.find_by!(id: @params[:id], vacancy_id: @params[:vacancy_id])
  end
end
